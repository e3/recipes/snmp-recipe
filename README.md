# snmp conda recipe

Home: "https://gitlab.esss.lu.se/epics-modules/snmp"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS snmp module
